---
title: markdown写作测试
tags:
  - markdown
categories:
  - 博客
comments: false
abbrlink: '809'
date: 2021-12-05 12:31:15
---

>写作专用
<!-- more -->
It's easy to install Hexo theme: you can choose to install the theme through npm, or download the source code of the theme in the `themes` directory under {% label info@site root directory %}.

{% tabs downloading-next %}
<!-- tab Using <code>npm</code> -->
If you're using Hexo 5.0 or later, the simplest way to install is through npm.

Open your Terminal, change to Hexo {% label info@site root directory %} and install NexT theme:

```bash
$ cd hexo-site
$ npm install hexo-theme-next
```
<!-- endtab -->

<!-- tab Using <code>git</code> -->
If you know about [Git](https://git-scm.com), you can clone the whole repository and update it in any time with `git pull` command instead of downloading archive manually.

Open your terminal, change to Hexo {% label info@site root directory %} and clone the latest master branch of NexT theme:

```bash
$ cd hexo-site
$ git clone https://github.com/next-theme/hexo-theme-next themes/next
```
<!-- endtab -->
{% endtabs %}

{% tabs Unique name,   %}
<!-- tab 111 -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab 222 -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab 333  -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}
<p>
bootstrap标签 label 【六大类型：default;primary;success;info;warning;danger;】 
<p>
</p>  
{% label default@default %} 默认  
{% label primary@primary %} 主要  
{% label success@success %} 成功
{% label info@info %} 信息  
{% label warning@warning %} 警告  
{% label danger@danger %} 危险    
</p>
bootstrap标签 label 【六大类型：default;primary;success;info;warning;danger;】   
{% note default %}
default
{% endnote %}

{% note primary %}
primary
{% endnote %}

{% note success %}
success
{% endnote %}

{% note info %}
info
{% endnote %}

{% note warning %}
warning
{% endnote %}

{% note danger %}
danger
{% endnote %}

The `prefers-color-scheme` CSS media feature is used to bring Dark Mode to all 4 schemes above, make sure your browser supports it.

{% caniuse prefers-color-scheme @ current,past_1,past_2,past_3,past_4,past_5 %}

Theme NexT automatically shows Dark Mode if the OS prefered theme is dark. It's supported by macOS Mojave, iOS 13 and Android 10 or later. Relevant docs:
[How to use Dark Mode on your Mac](https://support.apple.com/en-us/HT208976)
[Use Dark Mode on your iPhone, iPad, or iPod touch](https://support.apple.com/en-us/HT210332)
[Dark theme | Android Developers](https://developer.android.com/guide/topics/ui/look-and-feel/darktheme)


### Keep Up Indentation

6
{% tabs keep-up-indentation %}
<!-- tab 5 → -->
5

4
<!-- endtab -->

<!-- tab 4 -->
3

{% subtabs 2-spaces-indents %}
<!-- tab {% label danger@Nothing Happen %}-->
2
<!-- endtab -->

<!-- tab {% label success@Working Normally %}-->
1
<!-- endtab -->
{% endsubtabs %}

<!-- endtab -->
{% endtabs %}
